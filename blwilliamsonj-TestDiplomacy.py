#!/usr/bin/env python3
# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read 
    # ----

    def test_read_1(self):
        s = ["A Madrid Hold"]
        init = diplomacy_read(s)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]}}
        
        self.assertEqual(init, test)

    def test_read_2(self):
        s = ["A Madrid Hold",
             "B Barcelona Move Madrid",
             "C London Support B"]
        init = diplomacy_read(s)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]}}
        
        self.assertEqual(init, test)

    def test_read_3(self):
        s = ["A Madrid Hold",
             "B Barcelona Move Madrid"]
        init = diplomacy_read(s)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]}}
        
        self.assertEqual(init, test)

    def test_read_4(self):
        s = ["A Madrid Hold",
             "B Barcelona Move Madrid",
             "C London Support B",
             "D Austin Move London"]
        init = diplomacy_read(s)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]},
                "D":{"city": "Austin",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "London"]}}
        
        self.assertEqual(init, test)

    def test_read_5(self):
        s = ["A Madrid Hold",
             "B Barcelona Move Madrid",
             "C London Move Madrid"]
        init = diplomacy_read(s)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]}}
        
        self.assertEqual(init, test)
    
    def test_read_6(self):
        s = ["A Madrid Hold",
             "B Barcelona Move Madrid",
             "C London Move Madrid",
             "D Paris Support B"]
        init = diplomacy_read(s)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "D":{"city": "Paris",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]}}
        
        self.assertEqual(init, test)
    
    def test_read_7(self):
        s = ["A Madrid Hold",
             "B Barcelona Move Madrid",
             "C London Move Madrid",
             "D Paris Support B",
             "E Austin Support A"]
        init = diplomacy_read(s)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "D":{"city": "Paris",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]},
                "E":{"city": "Austin",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "A"]}}
        
        self.assertEqual(init, test)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        init = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]}}
        stat = diplomacy_eval(init)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]}}
        
        self.assertEqual(stat, test)

    def test_eval_2(self):
        init = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]}}
        stat = diplomacy_eval(init)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Hold", ""]},
                "B":{"city": "Madrid",
                     "num_supporting": 1,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]}}
        
        self.assertEqual(stat, test)

    def test_eval_3(self):
        init = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]}}
        stat = diplomacy_eval(init)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Move", "Madrid"]}}
        
        self.assertEqual(stat, test)

    def test_eval_4(self):
        init = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]},
                "D":{"city": "Austin",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "London"]}}
        stat = diplomacy_eval(init)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Support", "B"]},
                "D":{"city": "Austin",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Move", "London"]}}
        
        self.assertEqual(stat, test)

    def test_eval_5(self):
        init = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]}}
        stat = diplomacy_eval(init)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Move", "Madrid"]}}
        
        self.assertEqual(stat, test)

    def test_eval_6(self):
        init = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "D":{"city": "Paris",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]}}
        stat = diplomacy_eval(init)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Hold", ""]},
                "B":{"city": "Madrid",
                     "num_supporting": 1,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Move", "Madrid"]},
                "D":{"city": "Paris",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]}}
        
        self.assertEqual(stat, test)

    def test_eval_7(self):
        init = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "D":{"city": "Paris",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]},
                "E":{"city": "Austin",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "A"]}}
        stat = diplomacy_eval(init)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 1,
                     "dead": True,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 1,
                     "dead": True,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Move", "Madrid"]},
                "D":{"city": "Paris",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]},
                "E":{"city": "Austin",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "A"]}}
        
        self.maxDiff = None
        self.assertEqual(stat, test)
    
    def test_eval_8(self):
        # Armies move past each other
        
        init = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Barcelona"]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]}}
        stat = diplomacy_eval(init)
        test = {"A":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Barcelona"]},
                "B":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Madrid"]}}
        
        self.assertEqual(stat, test)
    
    def test_eval_9(self):
        # Army that is supporting another army while being supported itself
        # Supporting army defeats the attacker
        
        init = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "A"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]},
                "D":{"city": "Paris",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Barcelona"]}}
        stat = diplomacy_eval(init)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 1,
                     "dead": False,
                     "move": ["Support", "A"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]},
                "D":{"city": "Paris",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Move", "Barcelona"]}}
        
        self.assertEqual(stat, test)
    
    def test_eval_10(self):
        # Army that is supporting another army while being supported itself
        # Supporting army defeats the attacker
        
        init = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "A"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "D"]},
                "D":{"city": "Paris",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Barcelona"]}}
        stat = diplomacy_eval(init)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Support", "A"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "D"]},
                "D":{"city": "Barcelona",
                     "num_supporting": 1,
                     "dead":False,
                     "move": ["Move", "Barcelona"]}}
        
        self.assertEqual(stat, test)
    
    def test_eval_11(self):
        # Army that is supporting another army while being supported itself
        # Supporting army defeats the attacker
        
        init = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Barcelona",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Paris"]}}
        stat = diplomacy_eval(init)
        test = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Hold", ""]},
                "B":{"city": "Paris",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Move", "Paris"]}}
        
        self.assertEqual(stat, test)

    # -----
    # print
    # -----

    def test_print_1(self): # change these to dictionaries later

        "A Madrid Hold\nB Barcelona Move Madrid\nLondon Support B\n"
        
        w = StringIO() 
        d = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Hold", ""]},
                "B":{"city": "Madrid",
                     "num_supporting": 1,
                     "dead": False,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]}}
        diplomacy_print(w, d)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_print_2(self):
        w = StringIO()
        d = {"A":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Hold", ""]},
                "B":{"city": "Madrid",
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Move", "Madrid"]},
                "C":{"city": "London", 
                     "num_supporting": 0,
                     "dead": True,
                     "move": ["Move", "Madrid"]}}
        
        diplomacy_print(w, d)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_print_3(self):
        w = StringIO()
        d = {"A":{"city": "Madrid",
                     "num_supporting": 1,
                     "dead": True,
                     "move": ["Hold", ""]},  
                "B":{"city": "Barcelona",
                     "num_supporting": 1,
                     "dead": True,
                     "move": ["Move", "Madrid"]}, 
                "C":{"city": "Madrid",
                     "num_supporting": 0, 
                     "dead": True,
                     "move": ["Move", "Madrid"]},
                "D":{"city": "Paris",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "B"]},
                 "E":{"city": "Austin",
                     "num_supporting": 0,
                     "dead": False,
                     "move": ["Support", "A"]}}


        diplomacy_print(w, d)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


    # -----
    # solve 
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
