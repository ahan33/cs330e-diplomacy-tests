# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = StringIO("A Austin Move Houston\nB Dallas Hold")
        i = diplomacy_read(s)
        self.assertEqual(i[0],  "A Austin Move Houston")
        self.assertEqual(i[1],  "B Dallas Hold")


    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval(['A Madrid Support E','B Barcelona Move Madrid','C London Move Madrid','D Paris Hold','E Austin Move Paris'])
        self.assertEqual(v['A'], '[dead]')
        self.assertEqual(v['B'], '[dead]')
        self.assertEqual(v['C'], '[dead]')
        self.assertEqual(v['D'], '[dead]')
        self.assertEqual(v['E'], '[dead]')

    def test_eval_2(self):
        v = diplomacy_eval(['A Madrid Hold','B Barcelona Move Madrid','C London Move Madrid','D Paris Support B','E Austin Support A'])
        self.assertEqual(v['A'], '[dead]')
        self.assertEqual(v['B'], '[dead]')
        self.assertEqual(v['C'], '[dead]')
        self.assertEqual(v['D'], 'Paris')
        self.assertEqual(v['E'], 'Austin')

    def test_eval_3(self):
        v = diplomacy_eval(['C Tokyo Move Shanghai','J Shanghai Move Tokyo'])
        self.assertEqual(v['C'], 'Shanghai')
        self.assertEqual(v['J'], 'Tokyo')


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {'A':'[dead]','B':'Austin'})
        self.assertEqual(w.getvalue(), "A [dead]\nB Austin\n")


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Support E\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Hold\nE Austin Move Paris")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_3(self):
        r = StringIO("C Tokyo Move Shanghai\nJ Shanghai Move Tokyo")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "C Shanghai\nJ Tokyo\n")

# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()